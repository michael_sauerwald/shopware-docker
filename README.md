**SHOPWARE DEMO SHOP**

**Installation:**  
Copy the latest Shopware One-File Installer from  
https://de.shopware.com/download/   
to shopware-docker/html


Start the docker environment from project directory.  
`docker-compose up`

Go to shopware.localhost and follow the instructions

After stopping of the animation do a reload

On the database screen enter:  
server:mysql  
user: shopware  
database: shopware  
password:secret  

