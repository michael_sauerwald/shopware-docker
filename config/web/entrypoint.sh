#!/bin/bash

copy /var/www/patches/* /var/www/html/

chown -R www-data:www-data /var/www/

/usr/sbin/php-fpm7.1 && apachectl -D FOREGROUND

tail -f /dev/null
