<?php return array (
    'db' =>
        array (
            'host' => 'mysql',
            'port' => '3306',
            'username' => 'shopware',
            'password' => 'secret',
            'dbname' => 'shopware',
        ),
    'csrfProtection' => [
        'frontend' => false,
        'backend' => false
    ],
    'phpsettings' => [
        'error_reporting' => E_ALL & ~E_USER_DEPRECATED,
        'display_errors' => 1,
        'date.timezone' => 'Europe/Berlin',
    ],
    'snippet' => [
        'readFromDb' => true,
        'writeToDb' => true,
        'readFromIni' => false,
        'writeToIni' => false,
        'showSnippetPlaceholder' => true,
    ],
    'mail' => array(
        'type' => 'smtp',
        'host' => 'mailcatcher',
        'port' => 1025,
    ),
    'front' => [
        'noErrorHandler' => true,
        'throwExceptions' => true,
        'showException' => true
    ],
    'httpcache' => [
        'enabled' => false,
        'debug' => true,
        'default_ttl' => 0,
        'private_headers' => ['Authorization', 'Cookie'],
        'allow_reload' => false,
        'allow_revalidate' => false,
        'stale_while_revalidate' => 2,
        'stale_if_error' => false,
        'cache_cookies' => ['shop', 'currency', 'x-cache-context-hash'],
    ],
    'trustedProxies' => ['127.0.0.1'],
    'template' => [
        'forceCompile' => true,
    ],
    'session' => [
        'locking' => false
    ],
    'es' => [
        'prefix' => 'sw_shop',
        'enabled' => false,
        'number_of_replicas' => 0,
        'number_of_shards' => 1,
        'wait_for_status' => 'green',
        'client' => [
            'hosts' => [
                'localhost:9200'
            ]
        ]
    ],

    //   'httpcache' => [
    //        'enabled' => true,
    //        'debug' => true,
    //        'default_ttl' => 0,
    //        'private_headers' => ['Authorization', 'Cookie'],
    //        'allow_reload' => false,
    //        'allow_revalidate' => false,
    //        'stale_while_revalidate' => 2,
    //        'stale_if_error' => false,
    //        'cache_cookies' => ['shop', 'currency', 'x-cache-context-hash'],
    //        'storeClass' => include 'custom/plugins/SwagEssentials/RedisStore/loader.php',
    //        'redisConnections' => [
    //            ['host' => 'localhost', 'port' => 6379, 'dbindex' => 0]
    //        ]
    //    ],
    //    'model' => [
    //        'redisHost' => '127.0.0.1',
    //        'redisPort' => 6379,
    //        'redisDbIndex' => 0,
    //        'cacheProvider' => 'redis'
    //    ],
    //    'cache' => [
    //        'backend' => 'redis', // e.G auto, apcu, xcache
    //        'backendOptions' => [
    //            'servers' => array(
    //                array(
    //                    'host' => '127.0.0.1',
    //                    'port' => 6379,
    //                    'dbindex' => 0
    //                ),
    //            ),
    //        ],
    //    ]

);